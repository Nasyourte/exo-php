<?php
include('../model/Gens.php');
?>

<ul>
<?php
    foreach (getGensEtFleur() as $gens) {
?>
        <li>
        <?= $gens['nom']; ?>:
            <ul>
                <?php
                foreach ($gens['fleurs'] as $fleur) {
                ?>
                    <li>
                        <a href="<?= $fleur['lien_wiki']; ?>">
                            <?= $fleur['nom_vulgaire']; ?> (<?= $fleur['nom_latin']; ?>)
                        </a>
                    </li>
                    
                <?php
                }
                ?>
            </ul>
        
        </li>
<?php
    }
?>
</ul>