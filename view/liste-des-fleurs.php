<?php
include('../model/Fleur.php');
?>

<ul>
    <?php
    foreach (getAllFleur() as $fleur) {
        ?>

    <li>
        <a href="<?= $fleur['lien_wiki']; ?>">
            <?= $fleur['nom_vulgaire']; ?> (<?= $fleur['nom_latin']; ?>)
        </a>
    </li>

    <?php
    }
    ?>
</ul>