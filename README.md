# exo php

## associer des gens et des fleurs
(parce que pourquoi pas)

## model des données

┌────────────┐              ┌─────────────────────┐
│Gens        ├──────────────|Fleur                │
├────────────┤ *          * ├─────────────────────┤
│+nom: String│              │+nom_vulgaire: String│
└────────────┘              │+nom_latin: String   │
                            │+lien_wiki: String   │
                            └─────────────────────┘

Un gens peut avoir plusieurs fleurs.
Une fleur peut appartenit à plusieurs gens.

### table de liaison

Pour faire l'association *-*.

Il faut créer une table "intermédiaire" (de liaison) et y stocker l'id de Gens et l'id de la fleur.

┌─────────────┐
│gens_fleur   │
├─────────────┤
│id_gens: int │
│id_fleur: int│
└─────────────┘

## a faire
 - ecrire le code là ou il y a des '???'
 - créer les fichiers qui manquent (s'il en manque)
 - ecrire le reste du code là ou il manque
 - vérifier que tout marche

 ## dans l'ordre
 1. faire marcher le formulaire pour créer des gens
 2. vérifier dans la bdd
 3. faire marcher le formulaire pour créer des fleurs
 4. vérifier dans la bdd
 5. faire marcher l'affichage des gens et des fleurs
 6. faire marcher l'affichage gens et fleur.
 7. ajouter un formulaire avec 2 select - option
 8. les options du premier select sont les nom des gens et leur valeur son les id des gens
 9. les options du deuxieme select sont pour les fleurs (value = id de la fleur)
 10. quand le formulaire est valider vérifier que l'affichage gens / fleur se met bien à jour
